function InfoSend()
{
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.withCredentials = true;
    xmlHttpRequest.open('POST', 'https://api.slapform.com/dgopadakakv3@gmail.com', true);
    let formData = new FormData(document.forms.PersonalData);
    xmlHttpRequest.send(formData);
    xmlHttpRequest.upload.onprogress = function(event)
    {
        console.log(`Отправлено ${event.loaded} из ${event.total}`);
    };
    xmlHttpRequest.onloadend = function()
    {
        if (xmlHttpRequest.status == 200)
            console.log("Успешно");
        else
            console.log("Вызвано исключение: " + this.status);
    };
}

$(document).ready(function()
{
    PopUpHide();
    let ls = document.getElementById("LinkShow");
    ls.addEventListener("click", PopUpShow);
    let lh = document.getElementById("LinkHide");
    lh.addEventListener("click", PopUpHide);
    let sb = document.getElementById("subb");
    sb.addEventListener("click", InfoSend);
});

let stateObj =
{
    foo: "bar",
}

function PopUpShow()
{
    $("#popup1").show();
}

function PopUpHide()
{
    $("#popup1").hide();
}

function URLChange()
{
   	history.pushState(stateObj, "page 2", "form.html");
}